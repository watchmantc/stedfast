---
title: About
subtitle: About STEDFAST
date: 2020-05-20T04:01:30.000+00:00
meta: false
menu:
  main:
    weight: 50

---
> Come now, and let us reason together, saith the Lord (Isaiah 1:18a)

TC Howitt believes in Jesus Christ and the word of God. He has eyes that see and ears that hear, but also feet that walk and a mouth that speaks. This is his blog and he welcomes vigorous dialogue in the comments sections below each post.

***

### FAQ

**Q**: What does STEDFAST mean?

**A**: Same as the word _steadfast_: Faithful, true, devoted, unflinching, relentless, firmly fixed. See Hebrews 6:19, which is also the reason for the website's anchor icon.

**Q**: Why do you spell it _stedfast_ rather than _steadfast_?

**A**: _Stedfast_ is the older spelling as used in the Authorized Version of the Bible, also known as the King James Version (KJV), which I prefer as an English translation.

**Q**: Are you King James Only?

**A**: No.

**Q**: What's your denomination?

**A**: I'm a Bible-believing Christian. If you also believe that God is one and the eternal creator of all, that His only begotten Son Jesus Christ is Lord, that Christ died for our sins, was raised bodily by the Father, ascended to heaven and now reigns at the right hand of God, then we're brothers and sisters in the same faith called Christianity.