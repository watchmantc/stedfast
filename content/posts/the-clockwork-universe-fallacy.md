+++
aliases = ["/clockwork"]
date = 2020-04-22T07:00:00Z
tags = ["science"]
title = "The Clockwork Universe Fallacy"

+++
This is a picture of an automaton called the Digesting Duck:

![](/uploads/digesting-duck.png)

It looks, walks, eats and poops just like a duck. Is it a duck?

Descartes was so smitten with automatons -- machines that look like natural creations on the outside but operate like machines on the inside -- that he imagined the whole universe as a combination of mechanical interactions, a so-called clockwork universe.

Having killed God in our minds, as Nietzsche famously declared, we seek a replacement. Worship is a crucial aspect of human existence, and we all need to believe in something, anything, in order to think. Those who deny God's providence, or His very existence, were historically pagans, but today the heathen impulse surfaces most often in an even more delusional form: worship of our own cleverness through science and technology. For the modern idolater, wonderful technological products affirm their theory of a materialistic universe.

From this position of self-worship, the destiny of the whole world appears to be in our hands to either save or destroy, to either behave as good gods or evil ones. We take our creation in the image of God to be God Himself, and we confuse this shadow world with the world to come. We give ourselves a false choice based on a false worldview, and it's a cause of much confusion and suffering.

You should find it reassuring, not distressing, that God's word accords with our hearts. If you have a heart to feel anything at all, you already have regard for respecting natural life and caring for the earth, just as God does. Tend to the garden and care for the animals as responsible stewards.