+++
aliases = []
date = 2020-05-19T07:00:00Z
tags = ["church", "exegesis"]
title = "We're Doing Church All Wrong These Days"

+++
I believe Christians are doing church all wrong these days. I think we've gone astray, denominating ourselves into different sects rather than living by the example given in scripture for the New Testament church. We're in dire need of a biblical church restoration.

The Bible teaches that the church is one body with Christ as the head.

"So we, being many, are one body in Christ, and every one members one of another" (Rom 12:5).

"Now ye are the body of Christ, and members in particular" (1Co 12:27).

"Christ is the head of the church" (Eph 5:23b).

Here is the biblical model for the church:

As with your own personal body, which consists here and now of both soul and flesh, the church is both transcendent invisibly into eternity and manifest physically in the world. The universal, invisible church, since it is made up of members occupying space on earth, is partitioned physically into separate churches, one in each locality or city. These visible, local churches are completely independent from one another and overseen by their respective elders who are chosen from among their members. Nobody (and no body) whatsoever wields authority over any local church except Jesus Christ.

All church members in a locality assemble regularly in their homes, public spaces or anywhere else for that matter, often in smaller groups but sometimes the whole local church together in one place. The local church gathers for prayer, worship, reading of God's word, teaching, exhorting, healing, prophecy, singing hymns and playing spiritual songs, fellowship and breaking of bread.

Local churches in the Bible go by no name (the word "denomination" comes from a root meaning "to name"), but rather, they designate themselves by their locality alone. Take, for example, the biblical churches such as "the church in Corinth," "the saints in Christ Jesus which are at Philippi," "the church of Ephesus," etc. There is no exception to the rule that local churches are designated in the Bible by their city, and they're all unified under the one head of Jesus Christ.

"Now I beseech you, brethren, by the name of our Lord Jesus Christ, that ye all speak the same thing, and that there be no divisions among you; but that ye be perfectly joined together in the same mind and in the same judgment. For it hath been declared unto me of you, my brethren, by them which are of the house of Chloe, that there are contentions among you. Now this I say, that every one of you saith, I am of Paul; and I of Apollos; and I of Cephas; and I of Christ. Is Christ divided?" (1Co 1:10-13).

There is only one local church in each city. This is not ecumenism between churches in the same city, but rather unity into a single body organized by locality. And as 1 Corinthians 10:13 shows, even distinguishing a church as "of Christ" is sectarian if it separates itself from other Christians in the same locality. All Christians are united into one church, undivided in Christ spiritually and yet partitioned into separate churches by locality.

This begs the question: Who is a Christian, and thus, who is a member of the church? To answer that question, scripture tells us that all Christians are united under seven points of oneness:

"There is one body, and one Spirit, even as ye are called in one hope of your calling; one Lord, one faith, one baptism, one God and Father of all, who is above all, and through all, and in you all" (Eph 4:4-6).

1\. One body, the church.

2\. One Spirit, the Holy Spirit of God who indwells you.

3\. One hope of your calling, to eternal life with the Lord in glory.

4\. One Lord, Jesus Christ the Son of God.

5\. One faith, that Jesus died and rose from the dead for your salvation.

6\. One baptism, confessing with your mouth that Jesus is Lord.

7\. One God and Father of all, who is above all, and through all, and in you all.

Christians can disagree doctrinally on many subjects, but not in any of the seven points listed above. That means, for example, that Baptists, Pentecostals, Presbyterians and Methodists may all be sanctified through the sacrifice of the body of Jesus Christ. Even though they cannot agree on some issues that are extremely important, those points are not crucial for entering into the one body of Christ, His church. They're wrong for separating into denominations, but they're still Christians. Through the moving of the Holy Spirit and the building of the church, I pray that all Christians will move toward a right understanding of God's revealed truth, but these differences should never give an excuse for sectarianism. Splitting off into separate churches according to different doctrines and leaders is simply not part of God's design for building up His church, and in fact works to tear it down.

Local churches will take on the cultural traits and preferences of their members -- they will each have a personality, certain gifts and certain challenges according to the frame given by our Father and Creator, just like we do individually. Are you unhappy with your local church (assuming in fact that it is _the_ local church, in the Spirit of God)? Work within it for change, or failing in that, move to a different city.

Further, the Holy Spirit calls out certain individuals to serve as apostles to perform the traveling work of evangelism and church planting, which is completely separate from the life of the local churches but unified in spirit with them as members of the one universal church. The book of Acts chapter 13 shows us the primary example of this, where the Holy Spirit spoke to the teachers and prophets in the church in Antioch to set apart Barnabas and Saul (Paul), calling them out to serve as apostles in His mission to preach the gospel and plant churches in every locality across the region.

"Now there were in the church that was at Antioch certain prophets and teachers; as Barnabas, and Simeon that was called Niger, and Lucius of Cyrene, and Manaen, which had been brought up with Herod the tetrarch, and Saul. As they ministered to the Lord, and fasted, the Holy Ghost said, Separate me Barnabas and Saul for the work whereunto I have called them" (Acts 13:1-2).

From this verse, we can make a clear distinction between "the church" and "the work," the former consisting of the life of the body of Christ through its members, and the latter, the service undertaken by apostles to build up His church.

These apostles are not distinguished by their particular spiritual gifts of prophecy or healing, but rather by their appointment to the office and work of building up Christ's church as commissioned by the Holy Spirit. This is not to be confused with the modern notion of special charismatic anointing, or the earlier apostolic succession of church leaders. The word "apostle" comes from the Greek word ἀπόστολος _apostolos_, which means "one sent from or forth, a messenger, delegate."

Apostles hold no official authority in the local churches (unless they happen to serve as elders in their own local church back home), but they can and do carry spiritual influence in those local churches that are likewise in the Spirit. Any and all work performed by apostles is given freely to local churches, and neither apostles nor local churches are bound together in any official capacity.

Local churches need some amount of internal organization to conduct their meetings and establish order, every member working according to their spiritual and ministerial gifts. Apostles, on the other hand, while they may often work in companies of two or more individuals, eschew all forms of organization and official leadership. Apostles and local churches independently manage their own affairs as led by the Holy Spirit.

The local, visible churches in the Bible realized their oneness in Jesus Christ only through the work of the apostles, who traveled tirelessly between the churches to personally establish and build up that unity. We find no formal organization between the local churches in the Bible; the only concretization of their unity exists in the letters of the apostles to the churches, which serve today as evidence of God's design for His church, laid out for us like the plans He gave to Moses for building the tabernacle, and to David and Solomon for building His temple. In the Bible, we have the blueprints for building up God's church out of living stones based on the foundation of Jesus Christ, but we have left it behind for our own design.

In "A Concise History of the Catholic Church," Thomas Bokenkotter writes in regard to the biblical model for church cohesion through apostolic work rather than inter- and super-church organization, "It was only slowly and by degrees, however, that these informal and personal relations were translated into institutional and organizational ones."

Left to our own devices, without the guidance of God's word, we'll turn every gift from God into a monstrosity based on human pride rather than faithful humility. Human industry insidiously undermines faith. We never stopped building towers of Babel. Bokenkotter continues,

> The first steps in this direction occurred, it seems, when the bishops of a particular region began to meet in synods to discuss their common problems and adopt common solutions. The first synod of which we have knowledge took place in Asia between 160 and 175. Gradually certain churches assumed authority over other churches. Some of them acquired so-called metropolitan status, which elevated them over the churches of a province, while others – Rome, Alexandria, and Antioch, to be specific – acquired suprametropolitan status, by which they exercised a primacy over these metropolitan churches. Political factors were mainly responsible for these differentiations, the political preponderance. So the bishop of the capital of a Roman province was granted a certain superiority over the other bishops of that province; he had the right to convoke synods and to preside over the debates. The fourth canon of the Council of Nicaea (325) officially sanctioned this principle when it recognized the primacy of Rome, Alexandria, and Antioch.

Our manmade churches are the result of our lack of faith in God's promise and our preference for building the institutions of men instead. We must abandon our towers and return to God's plan for the church.

We need to establish and build up local churches where members meet in comfortable, casual, familial settings to share and grow in Christ's love. The pulpit-and-pew church service is completely unbiblical, and when we see in the Bible a single person preaching to a group of listeners (e.g., Paul preaching in Troas in Acts 20), it is the work of an apostle evangelizing that we're reading about and not the typical meeting of a church. The modern churches have at once tied down potential apostles into sectarian church roles as preachers -- men who are gifted for evangelism and could be used by God to plant and grow His church, but are instead designated as singular church leaders and pastors of their respective churches -- and also shut down the organic functions of the church meeting for the members who, rather than participating in mutual edification and growth, now expect a service to be performed for them on Sundays, where they sit and passively listen to a sermon. Sermons are certainly welcome in church meetings from time to time, when an evangelist and the church elders mutually feel a burden to preach a message to the gathered believers, but as a regular service, even the main feature of most church gatherings today, it finds no precedent in scripture.

The same can be said for the church buildings themselves and holding "Sunday service," unbiblical traditions that have so overwhelmed the true church model that many people now associate the churches not with the assembling of their members but with their buildings that open up for visitors on Sunday mornings. There's nothing wrong with meeting in a building for church on Sunday mornings, per se, provided it's not an insular gathering exclusive of other Christians, but it's definitely not prescribed by God, and it ought to involve much more than listening to a sermon and shaking the hands of fellow congregants.

Many churches today hold regular meetings in our homes for fellowship, teaching and breaking of bread, after the model shown at the end of Acts chapter 2. What we don't realize is, these home groups _are_ the local church (and whose assembling must not be forsaken)! The Sunday services in the church buildings, on the other hand, are manmade sectarian tradition. It is my belief that these traditions carried over from the Roman Catholic church, which in turn synthesized traditions from the Jewish synagogue, various idolatrous practices and even pan-Indian guru reverence, and that in the Reformers' focused efforts to reject all the doctrinal heresies in the Catholic Church, they stopped short of cutting loose from all of its unbiblical ceremonial trappings.

Despite the capabilities availed by the internet for instant communications, funding and resource delivery, we still need the traveling work of apostles more than ever to evangelize, plant and help grow biblical local churches. God has called apostles to personally deliver His message and plant churches, and that call has never been revoked or superseded by technology. This very message I'm writing now, if delivered online or even in a book, is fine as far as it goes, but it will never match the power of delivering it in person to those who have ears to hear it.

I'm still working through all the ramifications, for my faith and family first, and then for my work in and for the body of Christ. One interesting and formidable consequence of all this is that it reveals every city, in this country and in the world, to be a prime mission field for local church planting, desperately in need of spiritual restoration. It's a daunting task in that it challenges institutional churches and their established leadership, but it also promises to those who are in the faith that they all have a place in God's existing church right now -- they only need to meet up and celebrate that fact. God is building His church, and the gates of hell shall not prevail against it. I think a new revival could take the form of a local church movement, where God's plan for His church would reach free expression to do His will through the works of His faithful children.

I pray in all supplication for God's wisdom, strength and endurance in building His church, having faith in the Lord Jesus Christ that He will provide by His grace everything needed for the work for which the Spirit is calling me. I do not put my trust in men, neither the world nor Satan's lies, for in their devices masquerading as power dwell weakness, sickness, deception, folly and death; but I put all my trust in the Lord Jesus Christ, for in His Spirit is the power of God to eternal life that dwells within me.

Lord, may your kingdom come, your will be done, on earth as it is in heaven.