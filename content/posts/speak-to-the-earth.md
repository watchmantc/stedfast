+++
aliases = ["/evidence"]
date = 2020-05-29T07:00:00Z
tags = ["evidence"]
title = "Speak to the Earth"

+++
One man's plea to consider the evidence of God shown in all of creation:

    But ask now the beasts, and they shall teach thee;
    	And the fowls of the air, and they shall tell thee:
    Or speak to the earth, and it shall teach thee:
    	And the fishes of the sea shall declare unto thee.
    Who knoweth not in all these
    	That the hand of the Lord hath wrought this?
    In whose hand is the soul of every living thing,
    	And the breath of all mankind.
    
    Job 12:7-10

Here's the same message in a more didactic form:

> For the wrath of God is revealed from heaven against all ungodliness and unrighteousness of men, who hold the truth in unrighteousness; because that which may be known of God is manifest in them; for God hath shewed it unto them. For **the invisible things of him from the creation of the world are clearly seen, being understood by the things that are made, even his eternal power and Godhead; so that they are without excuse**. (Rom 1:18-20)