+++
aliases = []
date = 2020-03-27T07:00:00Z
tags = ["exegesis"]
title = "Unbelievable: Why God Chose the Foolishness of Preaching to Save Believers"

+++
The crucifixion of Jesus Christ is a hard pill to swallow.  Believe it or not, Jesus sacrificed his life to save yours, and three days later he rose from the dead.  That statement pushes the limits of credulity, and many intelligent people find it impossible to believe.

How could any self-respecting thinking person accept the following instruction?

"Trust in the Lord with all thine heart; and lean not unto thine own understanding. In all thy ways acknowledge him, and he shall direct thy paths. Be not wise in thine own eyes: fear the Lord, and depart from evil" (Pro 3:5-7).

The Bible says that God intended these claims to be outrageous and hard for any wise person to accept. Why would He do such a thing? In short, for those who like to take the message and run, the reason is this:

No man shall be glorified in preaching the cross.

Preachers will be mocked and hated in Jesus' name.

Associating oneself with Christ has always been burdensome.  Jesus said, "Think not that I am come to send peace on earth: I came not to send peace, but a sword" (Mat 10:34).  He meant that His word would divide believers from unbelievers, even to separate family members.  Indeed, Christians are called to separate themselves from worldly habits and the people who revel in those activities (1Jn 2:15).

The mark of a false preacher is being loved by the world.  Preaching the cross is a flesh-killer, just as God intended, breaking down the preacher while God builds him back up.

I know it can be hard to share the true gospel.  People hate God's word because they hate God and the truth about God (even while they don't _realize_ that fact), and they would rather have you just shut up already so they can continue in their wicked ways unimpeded (Mat 10:22).

The unsaved aren't even honest with themselves when it comes to this most essential subject, so they resort to mocking.  Many believers, facing such unpleasantness from unbelievers, just keep quiet.  And that's exactly what the unrepentant want.  But who lights a lamp and then puts it under a basket (Luke 11:33)?

Men are saved by hearing the preaching of the cross and believing, but first they are humbled and it is Christ who lifts them up.

Such a denial of self would be impossible without God, and this serves as a check-valve for rejecting those who would believe falsely and be saved falsely.  When the twelve disciples asked Jesus why He doesn't speak directly to those outside the kingdom of God, but only in parables, He said, "That seeing they may see, and not perceive; and hearing they may hear, and not understand; lest at any time they should be converted, and their sins should be forgiven them" (Mark 4:12).  The spiritual meaning of this teaching is that only those who have believed in Jesus Christ as their savior will receive the truth and be lifted up.

God makes the impossible possible, and the faith of believers proves it, to themselves and to anyone who would believe.

This is a trial for believers.  God, in His wisdom, catches the wise in their own craftiness this way: it pleases God to save people through the foolishness of preaching so that nobody can take pride in finding their own way to Him.

The war cry of humanists is, "I'm [good without God](https://americanhumanist.org/about/ "About the American Humanist Association")." Chances are, the humanist who believes this mantra is doing pretty well on their own in the world without acknowledging God.  God intervenes in the lives of the poor, the disenfranchised, the convicted criminals and the sick not because He will heal them now and bring them prosperity in the world, but because they already know they're broken and cannot do good.  So God offers them something better than the world, something they could never achieve on their own: inheritance in the kingdom to come.

That's why Jesus said, "It is easier for a camel to go through the eye of a needle, than for a rich man to enter into the kingdom of God" (Mat 19:24).

If you're called to be a disciple of Christ -- if you're called to be used as one of His servants to preach the word -- you will be mocked and hated for it.  You can always be hated and mocked for the wrong reasons, but if you're preaching right, it's a guarantee.  Jesus said to His disciples, "If the world hate you, ye know that it hated me before it hated you" (Jhn 15:18).  What's the right response to scoffing and hatred?  Shake off the dust and move on, then keep preaching to others who may have ears to hear and hearts to receive God's truth.

Jesus deliberately turned away thousands of people through the offense of the gospel He preached, making it clear that to follow Him means to give up your self-interest and suffer in this world (Jhn 6:66, Mat 16:24-25).

Evangelists are called to raise their voice like a trumpet and shout loud to the people about our true condition, and our great hope.  Like it or not, that's how God has chosen to save His people.  Many aren't gonna like what they hear, but a precious remnant will be saved.