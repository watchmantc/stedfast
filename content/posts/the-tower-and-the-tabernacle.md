+++
aliases = ["/tntbook"]
date = 2020-05-29T08:00:00Z
tags = ["book", "exegesis", "technology"]
title = "The Tower and the Tabernacle"

+++
The book will be titled _The Tower and the Tabernacle: Technology in Light of the Bible_.

    Let thy work appear unto thy servants,
      And thy glory unto their children.
    And let the beauty of the Lord our God be upon us:
      And establish thou the work of our hands upon us;
    Yea, the work of our hands establish thou it.
    
    Psalm 90:16-17

That's from the oldest psalm, and the only one attributed to Moses. Moses was, among many other things, God's foreman overseeing the construction of the tabernacle, His moveable temple. God gave Moses the blueprints and entrusted him with building the tabernacle according to that plan.

This book will be about two ways of looking at technology: our way and God's way.

Technology is not neutral. It is inextricably linked to our desires. The things we make manifest our intentions and pass along those intentions.

Technology has agency. In its pursuit of efficiency -- expediency without regard to God's will -- technology on its own can express fleshly impulses such as idolatry, pride, selfishness and lust. On the other hand, prayerfully seeking techniques for glorifying God, building His church and evangelizing Christ, is holy and biblically affirmed -- and if we follow this path, the Lord will provide abundantly (Mat 6:31-34).

Genesis 1:26-28 describes how God created man after His likeness, in His own image, to replenish and subdue the earth and to have dominion over every living thing. Technologies comprise the fruit of the techniques of man's dominion, the methods and tools we as creatures of God use to create other things in our own image. But left unchecked by God's word, man's technique will always yield rotten fruit.

The biblical accounts of the tower of Babel and the tabernacle of Moses represent two very divergent kinds of technology, and a survey of the manmade things described in the Bible will reveal the distinction between these two kinds of technology: on the one hand, contrivances used to serve man rather than God, here called _Tower Technique_, and on the other hand, methodologies used to faithfully serve God, here called _Tabernacle Technique_. We'll use this distinction as a guide for assessing technology and to determine what is that good, and acceptable and perfect will of God in these matters (Rom 12:2).

In our fallen state, we tend to favor Tower Technique in vain to reach heaven (Gen 11:4), while God's will is to have us worship and rely on Him using Tabernacle Technique. Unregenerate man strives upward with technology to reach and supplant God, to make a name for himself as a god unto himself, when he should be dropping to his knees and praying for guidance. Man pridefully reaches up with worldly knowledge and artifice rather than humbly trusting God through spiritual communion. Technological man launches rockets into space to send robots and explorers into the outer darkness without realizing that the way heavenward is instead to be first buried as a seed, dead to sin, planted with Christ, and then raised in newness of life, as a new creature, to be lifted up to righteousness in God's glory (Rom 6:4-11, 2Co 5:17).

_Lord, direct and bless the work of our hands according to your will_.