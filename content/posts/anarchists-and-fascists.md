+++
aliases = ["/anarchists"]
date = 2020-06-11T07:00:00Z
tags = ["culture"]
title = "Anarchists and Fascists"

+++
The black-hooded people in the streets calling themselves anarchists almost always subscribe to a vision of society -- a simple, idealistic and extremely naive vision -- that life would be perfect if people could just get along without any form of authority.

People actually tried to realize this vision in Catalonia and Zaragoza, where anarchists voluntarily lived by their anarchist principles and thrived for a while. Sadly, anarchists, once they organize into action groups, always end up resorting to violence, which is different than the sociopolitical idea of Anarchism. Their ideal view of humanity stems from their lack of faith in God, which itself often occurs in the absence of a godly biological father. And their total, obvious failure to live to up their own ideals for human behavior says something about our human nature without God and Jesus Christ.

That would be one way to sum up the far-left. People may think Communism is as far to the left that you can go, but the most-leftist left is Anarchism. You can go no further left, logically, than denying the legitimacy of every form of authority. The far-left is exactly where Adam went following the advice of the serpent, by way of the woman.

On the other hand, the far-right tactic of strongman nation-leading is exactly where Cain went. Fascism is on the other side of this left-right axis. Anarchism is principally a rebellion against God (with murder against brother following after it), and Fascism is principally a murder against brother (with rebellion against God following after it). I don't think it is coincidence that these are the first and second transgressions of man.

The far-left may lack faith in God, but the far-right hates his brother despite his professed faith in God. Which is worse? In God's eyes, they both reject the gospel of Jesus Christ, who affirmed the greatest commandment when asked the question, "Master, which is the great commandment in the law?"

> Jesus said unto him, Thou shalt love the Lord thy God with all thy heart, and with all thy soul, and with all thy mind.
>
> This is the first and great commandment.
>
> And the second is like unto it, Thou shalt love thy neighbour as thyself.
>
> On these two commandments hang all the law and the prophets.
>
> Matthew 22:36-40