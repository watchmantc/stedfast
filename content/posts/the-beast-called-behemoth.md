+++
aliases = ["/behemoth"]
date = 2020-05-21T07:00:00Z
tags = ["exegesis"]
title = "The Beast Called Behemoth"

+++
Consider the beast called Behemoth in the Book of Job.

Was Behemoth an elephant, a hippopotamus (which literally means "sea horse," incidentally), a dinosaur (e.g. brachiosaurus) or some kind of mythical monster? Nobody knows for sure. This open issue leads people to either question the literalism or inerrancy of the bible, or to take this as proof for a Young-Earth theory that includes people coexisting with dinosaurs within the last six thousand years, giving fuel to the creationism versus evolution debate. It is stunning that the identity of this animal can have such a profound effect on humanity's understanding of cosmology, but there you have it. Here's the verse that gets everyone all excited, so to speak:

> He moveth his tail like a cedar: the sinews of his stones are wrapped together (Job 40:17).

We know that the animal is big, yet gentle if left unprovoked, and grazes on grass like an ox. That could be any of the creatures I listed, though the brachiosaurus strips (or more properly, stripped) leaves from trees and can't really chew.

But a tail like a cedar? Surely this can't be an elephant or hippo. They've got spindly or stumpy little tails. Some have resorted to claiming the word "cedar" here in Hebrew means "cedar branch." That's hardly compelling; it doesn't fit with the rest of the description of this enormous creature. Are we left with the dinosaur, then?

Look at that verse again. God's talking about a tail that _moves_ like a cedar. So we're not talking about the appendage's literal size, but rather a figurative impression of its movement.

Behemoth is indeed an elephant, I say. I say this knowing that this idea is fully disputed and loudly mocked due to the size of the elephant's tail. Could "tail" refer to another body part, and if so, which part? It could hardly be the elephant's trunk, since a trunk is quite the opposite of a tail.

I'm afraid to report that the "tail" of Behemoth that moves like a cedar is his prodigious penis, which for elephants is about six feet long in total and when erect protrudes from the body in excess of three feet. The ESV translation says "He makes his tail stiff like a cedar" (Job 40:17 ESV). If you accept my hypothesis, the ESV is downright pornographic here!

What could it mean that, in the same verse, God says "the sinews of his stones are wrapped together?" The KJV keeps the word "stones" while almost every other translation (NIV, ESV, NASB and others) has it as "thighs." That's unfortunate. The "stones" are his testicles bound up deep within his body near his kidneys.

Unlike most land mammals, the elephant's testicles don't descend. As Aristotle comments in The History of Animals (350 BC): "the testicles are not visible, but are concealed inside in the vicinity of the kidneys." The elephant has been witnessed to swim up to 30 miles at a stretch, and thus must keep his testicles warm rather than cool.

As for God's intention in describing these qualities of the animal, he makes it clear in the surrounding verses that Behemoth has nothing to worry about, neither in his manhood, so to speak, nor in his defenses against testicular evisceration. In context, God says Behemoth's bones are like brass, bars of iron, that the animal carries his strength in his loins, that his force is in the navel of his belly.

The language is obscured due to taboo. "Tail" and "stones" are both euphemisms, as attested to by English and Hebrew etymologies. The etymology of "penis" comes from Latin "penis," which is itself derived from the word for "tail." As for "stones," the word in English remains to this day as a euphemism for testicles. Even more interesting is the fact that the word translated here as "stones," פּחד _pachad_, is also the Hebrew word for an animal's testicles. Compare this euphemism with the word "nakedness" used throughout the Bible in place of "genitals."

I affirm the inerrancy of the Bible, but not my own inerrancy in interpreting the Bible, which includes my hypothesis here. In other words, this is my opinion.

Much love to all of you, and may you never encounter an elephant in heat. In fact, don't even Google that subject.