+++
aliases = []
date = 2020-04-12T08:38:17Z
tags = ["exegesis", "easter"]
title = "A Matter of Timing"

+++
Happy Resurrection Day.

The exact timing of Jesus' crucifixion, burial and resurrection is a matter that tends to concern only academic Bible exegetes and atheist detractors, and rarely concerns true believers. That means it's mostly a "who cares" kind of issue when it comes to Christian observance of this holiday. Still, I think it can be edifying to talk about it.

The gospel accounts are quite confusing to modern-day western readers for a number of reasons. For one thing, the Jews considered calendar days to begin at sunset rather than at midnight. A second issue regards how one counts full days. A third complication involves the double use of the term "Sabbath." A final issue regards the specific meaning of "Passover."

Here's my summary interpretation of various biblical passages:

Jesus was crucified near dusk on Wednesday, the 14th day of Nisan, the day of preparation, when the unblemished Paschal Lamb is slaughtered -- that evening being the beginning of the Passover High Sabbath on Thursday -- the 15th day of Nisan. So Christ was dead and in the tomb for all of Thursday, Friday and Saturday, and then raised from the dead near dawn on Sunday, the Lord's Day.

"For as Jonas was three days and three nights in the whale's belly; so shall the Son of man be three days and three nights in the heart of the earth" (Matt 12:40).

Readers of scripture may overlook that "Passover" can mean any of the feast days or the whole weeklong holiday. This same ambiguity exists in our usage of the term today.

More crucially, I believe the church has been very confused over the fact that both Thursday, the 15th day of Nisan, and that following Saturday are called "Sabbath." One is the High Sabbath and the other the weekly sabbath.

According to my interpretation, then, Good Friday (mostly a Roman Catholic holiday) should actually be Good Wednesday.

I could be wrong about this. I've been wrong lots of times, but this is my current understanding. Scriptural references and defenses available upon request (I spare you all that here for brevity's sake). If you can help me interpret scripture more accurately, I'm all ears.

God bless you all, believers and unbelievers alike.