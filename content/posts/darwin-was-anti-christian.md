+++
aliases = ["/darwin"]
date = 2020-05-05T07:00:00Z
tags = ["atheism", "science"]
title = "Darwin was Anti-Christian"

+++
In his public writings, Charles Darwin gave the impression that he had nothing against God, and in fact believed in God as any good Christian does. This impression has given Darwinists support for claiming that the theory of evolution that Darwin promoted, and on a related subject, the materialist, godless theory of beginnings, have nothing to do with being anti-Christian but instead stem from a logical, evidence-based, scientific approach to nature using observation and reason.

The truth is just the opposite. Like all the world's other celebrations of sin, at the center of evolutionary theory lies an illogical, blind, unreasonable, unscientific rebellion against God.

In his private correspondence, Darwin self-identified as an agnostic. He never expressed these views in formal writings for two main reasons: to avoid offending his family, particularly his devout wife, and more tellingly, because he figured he could more effectively wage a campaign against Christianity indirectly by promoting the secular theory of evolution. He was right about that.

In 1880, two years before his death, Darwin wrote a letter to atheist Edward Aveling saying, "it appears to me (whether rightly or wrongly) that direct arguments against christianity \[sic\] and theism produce hardly any effect on the public... It has, therefore, been always my object to avoid writing on religion."

And the following is a terse private letter he wrote to a Christian fan of his, also in 1880:

> I am sorry to have to inform you that I do not believe in the Bible as a divine revelation & therefore not in Jesus Christ as the son of God. Yours faithfully, Ch. Darwin.

![](/uploads/letter.jpg)

This letter sold at auction for $197,000 in 2015. Note that it ends with "Yours faithfully."

Every single one of us has faith, even agnostics and atheists. The only question is, Is your faith in the living God or is your faith in something else? Is it your significant other? Theoretical scientists? Your works? Celebrities? Philosophers? Yourself?

Is your faith based on evidence and substance or is it a blind faith based on theory and wishing upon a star?

Hebrews 11:1 says that faith in God "is the substance of things hoped for, the evidence of things not seen." The passage continues, "Through faith we understand that the worlds were framed by the word of God, so that things which are seen were not made of things which do appear" (Heb 11:3). These verses are talking about the evidence for God and his creation that believers know, evidence that man can never see without the help of the word of God as illumined by the Holy Spirit. The unregenerate souls who don't accept the word of God are left in the dark, with only their blind faith in their own understanding to lean on, separated from God, walking dead in their trespasses and sin.

Rather than be judged righteously by God, the unregenerate will faithfully accept any theories, no matter how dumb, if those theories help them continue living under their own control and desires, in their flesh, in sin. Darwin earnestly sought to bolster the theoretical support for denying God, and people of the world practically worship him for it.

Even atheists know John 3:16 ("For God so loved the world..."), but not many know what comes right after it in John 3:19-20:

"And this is the condemnation, that light is come into the world, and men loved darkness rather than light, because their deeds were evil. For every one that doeth evil hateth the light, neither cometh to the light, lest his deeds should be reproved."