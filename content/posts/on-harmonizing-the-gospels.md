+++
aliases = ["/harmonizing"]
date = 2020-05-08T07:00:00Z
tags = ["exegesis"]
title = "On Harmonizing the Gospels"

+++
Unbelievers complain about differences between the narratives given in the four gospels (the books of Matthew, Mark, Luke and John).  They think contradictions exist among these narratives, and they complain about "gospel harmonization" as if that were a bad thing, merely a way to smooth out the crinkles embedded in the biblical account of Jesus Christ.

Allow me to illustrate the hollowness of their complaints.  Here are three accounts of the same scene:

1\. A man walks into a bar.  There was only one other patron there, who hadn't paid his tab all month, camped out on a stool at the lotto machines downing the dregs of his glass of Bud.

2\. A rude man rushes into a bar.  He almost runs into the guy crossing the room with a full glass of beer, nearly spilling it all over the place.

3\. I walked into a bar and the place was deserted.  Seeing a woman sitting a table, I asked her where I could find the restroom and she pointed the direction with her pen.

Are these eyewitness testimonies contradictory?  No.  Let's harmonize these, shall we?

The first narrative is told from the perspective the owner, a woman sitting at a table working on the books.  The second is told by a cranky, lotto-playing customer.  The third is told by the man who walked into the bar looking for the bathroom.  The man crossing the room is the bartender, bringing a beer to the lotto guy.

Shifting limited perspectives bring a scene to life in the mind of the reader.  They show more than they tell, leading to a more direct and well-rounded perception of actual events.  We can learn about personalities, roles, interactions and motives in a subtle and powerful way.

Lawyers, judges, juries and cops know how this works with eyewitness testimonies.  Lots of good novelists and journalists know how this works.  Bible believers know how this works, too.  Gospel harmonization is a good thing, and a blessing.