+++
aliases = ["/judging"]
date = 2020-05-17T07:00:00Z
tags = ["exegesis", "judgment"]
title = "Jesus Says to Judge Righteously"

+++
When Jesus said, "Judge not, that ye be not judged" (Mat 7:1), He was warning about the consequences of hypocritical judgment, not against judgment itself. The next verse makes this clear:

"For with what judgment ye judge, ye shall be judged: and with what measure ye mete, it shall be measured to you again" (Mat 7:2).

Furthermore, Jesus then gives an explicit lesson on how to judge properly -- that is, without hypocrisy:

> And why beholdest thou the mote that is in thy brother's eye, but considerest not the beam that is in thine own eye? Or how wilt thou say to thy brother, Let me pull out the mote out of thine eye; and, behold, a beam _is_ in thine own eye? **Thou hypocrite, first cast out the beam out of thine own eye; and then shalt thou see clearly to cast out the mote out of thy brother's eye**. (Mat 7:3-5)

Another interesting point is raised in the next verse, which begins, "Give not that which is holy unto the dogs, neither cast ye your pearls before swine" (Mat 7:6a). Obeying this famous instruction requires that one judges who exactly are the dogs and swine.

Jesus also said, "Judge not according to the appearance, but **judge righteous judgment**" (Jhn 7:24).

That's a command to judge, and to judge in a certain way. What is righteous judgment? It is judgment in the Holy Spirit, discerning between truth and error according to sound doctrine. This kind of judgment is righteous and never hypocritical or superficial.

This kind of judgment is discernment, not condemnation. Condemnation is ultimately reserved for God Himself.

A line from the book of John is often invoked to discourage judgment -- "He that is without sin among you, let him first cast a stone" (Jhn 8:7b) -- but that's an unfortunate case of taking a passage out of context. Jesus is telling the Pharisees that the Mosaic Law is over and done, and along with it, the civil death penalty by stoning for certain sins. According to the New Covenant in Jesus Christ, God now takes care of all that for us (Jhn 8:10-12, Mat 5:38-39, Rom 12:17ff, Gal 3:13-26). This is fulfilled prophecy (Jer 31:31-34).

Paul wrote to the church at Corinth, "But the natural man receiveth not the things of the Spirit of God: for they are foolishness unto him: neither can he know them, because they are spiritually discerned. But **he that is spiritual judgeth all things**, yet he himself is judged of no man. For who hath known the mind of the Lord, that he may instruct him? But we have the mind of Christ" (1Co 2:14-16).

Paul had a gift for speaking the truth in boldness. He had the mind of Christ.

Paul also wrote to Timothy, "I charge thee therefore before God, and the Lord Jesus Christ, who shall judge the quick and the dead at his appearing and his kingdom; preach the word; be instant in season, out of season; reprove, rebuke, exhort with all longsuffering and doctrine" (2Ti 4:1-2).

Practically, righteous judgment means I judge according to the word of God, and my judgment leads me to preach the word of God. "Faith cometh by hearing, and hearing by the word of God" (Rom 10:17).

For this the lost try to judge the one preaching as being judgmental, which is endlessly ironic. It would be funny if it weren't so tragic. This verse bears repeating:

"But he that is spiritual judgeth all things, yet he himself is judged of no man" (1Co 2:15).

Jesus said, "Light is come into the world, and men loved darkness rather than light, because their deeds were evil. For every one that doeth evil hateth the light, neither cometh to the light, lest his deeds should be reproved" (Jhn 3:19-20).

That condemnation stings, but I didn't say it. Jesus said it.

I once was lost, but now I'm found. God's word comes on like a lightning strike and then stays with you like a lamp to your feet. That light illuminates the darkness and helps me to judge righteously, not from my own wisdom and righteousness, which is foolishness and filthy rags, but with the sound doctrine that comes from God's word. There's no better gift than this, and it's free. None of us deserve the gift, but God offers it to everyone out of His boundless treasury of love and mercy.

Act now, because this free offer won't last and supplies are limited.