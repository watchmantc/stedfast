+++
aliases = []
date = 2020-05-13T07:00:00Z
tags = ["atheism"]
title = "Why Some People Choose to Hate God"

+++
People make up excuses to reject the Bible because accepting its teachings is fundamentally convicting. If you acknowledge that the Bible is the true word of God, you've got some repenting to do, which means changing your life around to serve Jesus rather than yourself. That's a big change, and most people will choose to reject the call to the light and continue their retreat into darkness (Jhn 3:19, Mat 7:13).

One of the most common excuses for rejecting biblical Christianity, right after "all the churches are full of hypocrites" (yes, and they're full of liars, murderers and adulterers too -- and they've always got room for one more) is that the Bible recounts a lot of horrific human behavior as well as the terrifying wrath of God.

The fallenness of mankind is self-evident. We're immersed in depravity, suffering, corruption and death. Who can deny it? Further, this fact of sin frames the entire Old Testament and gives rise to the saving gospel of the New Testament.

God gave his law to us through Moses not so people would follow it perfectly, but so we would prove to ourselves that nobody is worthy to live up to it. "The law was our schoolmaster to bring us unto Christ" (Gal 3:24a).

Nobody has ever been able to fulfill God's law except Jesus Christ, who fulfilled the law and the prophecies perfectly (Mat 5:17).

The just punishment for breaking God's law is death (Rom 6:23). Many people find this unfair and nothing will sway them, ever.

For such haters of God, who are legion, Jesus had these words:

"Ye are of your father the devil, and the lusts of your father ye will do. He was a murderer from the beginning, and abode not in the truth, because there is no truth in him. When he speaketh a lie, he speaketh of his own: for he is a liar, and the father of it" (Jhn 8:44).

God gives up the children of wrath like so many withering branches cast into the fire and burned (Jhn 15:6, Mat 7:19).

Jesus Christ, who dwelled among us in the flesh and perfectly followed the will of the Father, took our sin upon himself and paid the due penalty by satisfying the full wrath of God in His sacrifice. Believers are justified through their faith in Christ, and not of themselves (Eph 2:8-9).

Through the atonement of Christ, God's children are like firebrands snatched from the blaze. So for that everlasting blessing, the blessed praise God for His mercy -- starting in this life and continuing through eternity.