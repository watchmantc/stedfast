+++
tags = ["culture"]
aliases = []
date = 2020-04-02T01:17:56.000+00:00
title = "A Pitiable Ignorance"

+++
Misconstruing the Bible, whether deliberately or on accident, is a tradition among American politicians that goes all the way back to the Continental Congress. To wit:

One member began his speech with the words, "Mr. President, there is an old and good book, which is not read as much as it ought to be -- I mean the Bible, sir -- which says, 'Of two evils we should always choose the least.'"

Hearing this, the Reverend Doctor Witherspoon, a signer of the Declaration of Independence, rose and said, "The gentleman will greatly oblige us, if he will refer to chapter and verse."

The chaplain to the US House of Representatives who conveyed this account, Ashbel Green, concludes, "Members of Congress since their debates have been in publick, have sometimes shown a pitiable ignorance, and at other times a lamentable profaneness, by affecting a familiarity with the sacred scriptures, or by grossly misapplying them."