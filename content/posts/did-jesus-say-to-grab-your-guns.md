+++
aliases = ["/guns"]
date = 2020-05-21T20:22:00Z
tags = ["exegesis", "culture"]
title = "Did Jesus Say to Grab Your Guns?"

+++
Check out Luke 22:35-53.

Three points about this passage.

First, two swords were enough. How is this supposed to be adequate for fighting off the multitudes?

Second, in the parallel passage in Matthew, Jesus says this to Peter after Peter attacks a servant of the high priest: "Put up again thy sword into his place: for all they that take the sword shall perish with the sword" (Matt 26:52).

Third, it's all about the fulfillment of prophecy. Note how Matthew records Jesus' explanation for having Peter put away his sword: "Thinkest thou that I cannot now pray to my Father, and he shall presently give me more than twelve legions of angels? But how then shall the scriptures be fulfilled, that thus it must be?" (Matt 26:53-54).

That's because, going back to Luke, Jesus explained this reason for acquiring swords: "For I say unto you, that this that is written must yet be accomplished in me, And he was reckoned among the transgressors: for the things concerning me have an end" (Luke 22:37).

The prophecy in this case is this: "Therefore will I divide him a portion with the great, and he shall divide the spoil with the strong; because he hath poured out his soul unto death: and he was numbered with the transgressors; and he bare the sin of many, and made intercession for the transgressors" (Isa 53:12b).

Finally, check out what Jesus said when the multitudes approached to arrest Him: "Then Jesus said unto the chief priests, and captains of the temple, and the elders, which were come to him, Be ye come out, as against a thief, with swords and staves?" (Luke 22:52).

Jesus is indicating that prophecy was thereby fulfilled when they reckoned Him to be among the transgressors, and that's because they were armed with swords -- albeit only two swords.

This passage isn't saying to gear up for a bloody battle, nor even to defend yourself with a literal sword.