+++
aliases = ["/supernatural"]
date = 2020-04-15T07:00:00Z
tags = ["evidence", "science"]
title = "The First Cause was Supernatural"

+++
Nature, if it has not eternally existed, came into being by a first cause, and that first cause was and is, by definition, supernatural -- above and beyond nature. Furthermore, it is folly to seek evidence for the supernatural in the material.

"While we look not at the things which are seen, but at the things which are not seen: for the things which are seen are temporal; but the things which are not seen are eternal" (2Co 4:18).

How do we come to know this unseen supernatural force?

"Now faith is the substance of things hoped for, the evidence of things not seen" (Heb 11:1).

You contain within yourself the potential to observe the evidence of things not seen. You already have a consciousness, you already have a sense of right and wrong, and you already have a boundless capacity for love and faith. Now you need to activate that faith, the faith that points to a hope that cannot fail because you can witness its real substance and trust its indisputable evidence, if only you'll have the Holy Spirit dwell within you to shine the true Light, which is Jesus Christ (Jhn 1:9).